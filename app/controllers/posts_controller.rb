# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :authenticate_user!, only: %i[create destroy update edit new]
  before_action :check_user, only: %i[destroy update edit]

  def index
    @posts = check_my_posts.page(params[:page]).per(5)
    @post = Post.new
  end

  def show
    @post = Post.find(params[:id])
    @comment = Comment.new
  end

  def new; end

  def edit
    @post = Post.find(params[:id])
  end

  def create
    post = Post.new(post_params)

    flash[:alert] = "Error while creating (#{post.errors.messages})" unless post.save

    redirect_to root_path
  end

  def update
    post = Post.find(params[:id])

    flash[:alert] = "Error while updating (#{post.errors.messages})" unless post.update(post_params)

    redirect_to post_path(post)
  end

  def destroy
    post = Post.find(params[:id])

    flash[:alert] = "Error while deleting (#{post.errors.messages})" unless post.destroy

    redirect_to root_path
  end

  private

  def post_params
    params.require(:post).permit(:user_id, :name, :body, :image)
  end

  def check_user
    post = Post.find(params[:id])

    redirect_to root_path if post.user_id != current_user.id
  rescue StandardError
    p 'The post is not found'

    redirect_to root_path
  end

  def check_my_posts
    if params[:my_posts]
      Post.my_posts(current_user)
    else
      Post.all
    end
  end
end
