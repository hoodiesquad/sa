# frozen_string_literal: true

class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_user

  def create
    comment = Comment.new(comment_params)

    flash[:alert] = "Error while commenting (#{comment.errors.messages})" unless comment.save

    redirect_to post_path(comment.post_id)
  end

  def destroy
    comment = Comment.find(params[:id])
    post_id = comment.post_id

    flash[:alert] = "Error while deleting (#{comment.errors.messages})" unless comment.delete

    redirect_to post_path(post_id)
  end

  private

  def comment_params
    params.require(:comment).permit(:user_id, :post_id, :body)
  end

  def check_user
    comment = Comment.find_by(id: params[:id])

    if comment
      redirect_to root_path if comment.user_id != current_user.id
    elsif params[:comment][:user_id].to_i != current_user.id
      redirect_to root_path
    end
  end
end
