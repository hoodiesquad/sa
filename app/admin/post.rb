# frozen_string_literal: true

ActiveAdmin.register Post do
  permit_params :name, :body, :user_id

  index do
    id_column
    column :name
    column :updated_at
    actions
  end
end
