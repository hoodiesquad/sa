# frozen_string_literal: true

ActiveAdmin.register User do
  permit_params :email, :nickname, :posts, :password, :password_confirmation

  index do
    id_column
    column :email
    column :nickname
    column :updated_at
    column :created_at
    actions
  end

  filter :email
  filter :created_at

  form do |f|
    f.inputs do
      f.input :email
      f.input :nickname
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end
end
