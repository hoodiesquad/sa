# frozen_string_literal: true

class Post < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy

  has_one_attached :image

  scope :my_posts, ->(user) { where(user_id: user.id) }
end
