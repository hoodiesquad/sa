FROM ruby:3.1.2
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
WORKDIR /sa
COPY Gemfile Gemfile.lock ./
RUN gem install bundler && bundle install
COPY . .
