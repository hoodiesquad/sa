# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
    3.1.2

* Database creation
    local: bundle exec rails db:create | bundle exec rails db:migrate 

* Deployment instructions
  * Docker init:
    * Comment out 20 and uncomment 21 lines of the database.yml file
    * docker compose up
    * docker compose run web bundle exec rails db:migrate
    * docker compose run web bundle exec rails db:seed
