require 'rails_helper'

RSpec.describe AdminUser, type: :feature do
  let(:admin_user) { create(:admin_user) }
  before { login_as(admin_user, scope: :admin_user) }

  describe 'creating a new user' do
    before do
      visit new_admin_admin_user_path
      fill_in 'Email*', with: 'test@example.com'
      fill_in 'Password*', with: 'password123'
      fill_in 'Password confirmation', with: 'password123'
      click_button 'Create Admin user'
    end

    it 'redirects to the index page and displays success message' do
      expect(page).to have_current_path(admin_admin_user_path(AdminUser.last))
      expect(page).to have_content('Admin user was successfully created.')
    end

    it 'creates a new user in the database' do
      expect(AdminUser.count).to eq(2)
      expect(AdminUser.last.email).to eq('test@example.com')
    end
  end
end
