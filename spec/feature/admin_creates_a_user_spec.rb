require 'rails_helper'

RSpec.describe 'Admin creates a new user', type: :feature do
  let(:admin_user) { create(:admin_user) }
  before { login_as(admin_user, scope: :admin_user) }

  describe 'creating a new user' do
    before do
      visit new_admin_user_path
      fill_in 'Email*', with: 'test@example.com'
      fill_in 'Password*', with: 'password123'
      fill_in 'Password confirmation', with: 'password123'
      click_button 'Create User'
    end

    it 'redirects to the index page and displays success message' do
      expect(page).to have_current_path(admin_user_path(User.last))
      expect(page).to have_content('User was successfully created.')
    end

    it 'creates a new user in the database' do
      expect(User.count).to eq(1)
      expect(User.last.email).to eq('test@example.com')
    end
  end
end
