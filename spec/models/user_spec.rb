# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it 'is valid with a name and an email and a password' do
      user = User.new(nickname: 'John', email: 'john@example.com', password: 'password')
      expect(user).to be_valid
    end

    it 'is valid with only a name and an email' do
      user = User.new(email: 'john@example.com', password: 'password')
      expect(user).to be_valid
    end

    it 'is not valid without password' do
      user = User.new(email: 'john@example.com')
      expect(user).to_not be_valid
    end

    it 'is not valid without email' do
      user = User.new(password: 'password')
      expect(user).to_not be_valid
    end
  end

  describe 'associations' do
    let(:user) { create(:user) }

    it 'user has many posts' do
      expect(user.posts).to be_an ActiveRecord::Associations::CollectionProxy
    end

    it 'user has many comments' do
      expect(user.comments).to be_an ActiveRecord::Associations::CollectionProxy
    end
  end
end
