require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe 'validations' do
    let(:user) { create(:user) }
    let(:post) { create(:post, user_id: user.id) }

    it 'is valid with body' do
      comment = Comment.new(body: 'Lorem ipsum', user_id: user.id, post_id: post.id)
      expect(comment).to be_valid
    end

    it 'is valid without params' do
      comment = Comment.new(user_id: user.id, post_id: post.id)
      expect(comment).to be_valid
    end

    it 'is not valid without user' do
      comment = Comment.new(post_id: post.id)
      expect(comment).to_not be_valid
    end

    it 'is not valid without post' do
      comment = Comment.new(user_id: user.id)
      expect(comment).to_not be_valid
    end
  end

  describe 'associations' do
    let(:user) { create(:user) }
    let(:post) { create(:post, user_id: user.id) }
    let(:comment) { create(:comment, user_id: user.id, post_id: post.id) }

    it 'post belong to user' do
      expect(comment.user).to be_an User
    end

    it 'post has many comments' do
      expect(comment.post).to be_an Post
    end
  end
end
