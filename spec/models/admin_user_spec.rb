# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AdminUser, type: :model do
  describe 'validations' do
    it 'is valid with an email and a password' do
      admin_user = AdminUser.new(email: 'john@example.com', password: 'password')
      expect(admin_user).to be_valid
    end

    it 'is not valid without password' do
      admin_user = AdminUser.new(email: 'john@example.com')
      expect(admin_user).to_not be_valid
    end

    it 'is not valid without email' do
      admin_user = AdminUser.new(password: 'password')
      expect(admin_user).to_not be_valid
    end
  end
end
