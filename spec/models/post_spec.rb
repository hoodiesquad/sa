# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  describe 'validations' do
    let(:user) { create(:user) }

    it 'is valid with name and body' do
      post = Post.new(user_id: user.id, name: 'name', body: 'Lorem ipsum')
      expect(post).to be_valid
    end

    it 'is valid without params' do
      post = Post.new(user_id: user.id)
      expect(post).to be_valid
    end
  end

  describe 'associations' do
    let(:user) { create(:user) }
    let(:post) { create(:post, user_id: user.id) }

    it 'post belong to user' do
      expect(post.user).to be_an User
    end

    it 'post has many comments' do
      expect(post.comments).to be_an ActiveRecord::Associations::CollectionProxy
    end
  end

  describe 'scope :my_posts' do
    let(:user) { create(:user) }

    it 'returns posts with user.id' do
      create_list(:post, 5, user_id: user.id)

      expect(Post.my_posts(user).size).to eq(5)
    end
  end
end
