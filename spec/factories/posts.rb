# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    user_id { 1 }
    name { 'test' }
    body { '123123' }
  end
end
