# frozen_string_literal: true

FactoryBot.define do
  factory :admin_user do
    email { 'test_admin@email.com' }
    password { 'password123' }
  end
end
