require 'rails_helper'

RSpec.describe 'Comments', type: :request do
  describe 'Non auth user' do
    let!(:user) { create(:user) }
    let!(:user_post) { create(:post, user_id: user.id) }
    let!(:user_comment) { create(:comment, user_id: user.id, post_id: user_post.id) }

    it 'Creating a comment without user' do
      expect do
        post comments_path, params: { comment: { body: 'non auth body', user_id: user.id, post_id: user_post.id } }
      end.to(change(Comment, :count).by(0))

      expect(response).to have_http_status(:redirect)
    end

    it 'Destroying a comment without user' do
      expect do
        delete comment_path(user_comment)
      end.to(change(Comment, :count).by(0))

      expect(response).to have_http_status(:redirect)
    end
  end

  describe 'auth user' do
    let!(:user) { create(:user) }
    let!(:user_post) { create(:post, user_id: user.id) }
    let!(:user_comment) { create(:comment, user_id: user.id, post_id: user_post.id) }
    let!(:another_user) { create(:user, email: 'test2@email.com') }
    let!(:another_user_comment) { create(:comment, user_id: another_user.id, post_id: user_post.id) }

    before :each do
      sign_in user
    end

    it 'Creating a comment with auth user' do
      expect do
        post comments_path, params: { comment: { body: 'auth body', user_id: user.id, post_id: user_post.id } }
      end.to(change(Comment, :count).by(1))

      expect(response).to have_http_status(:redirect)
    end

    it 'Destroying a comment with auth user' do
      expect do
        delete comment_path(user_comment)
      end.to(change(Comment, :count).by(-1))

      expect(response).to have_http_status(:redirect)
    end

    it 'Destroying a comment another auth user' do
      expect do
        delete comment_path(another_user_comment)
      end.to(change(Comment, :count).by(0))

      expect(response).to have_http_status(:redirect)
    end
  end
end
