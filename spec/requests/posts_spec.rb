require 'rails_helper'

RSpec.describe 'Posts', type: :request do
  describe 'Non auth user' do
    let(:user) { create(:user) }
    let(:user_post) { create(:post, user_id: user.id) }

    describe 'GET /index' do
      it 'should be render index view' do
        get root_path

        expect(response).to have_http_status(:success)
        expect(response).to render_template('posts/index')
      end
    end

    describe 'GET /posts/:id' do
      it 'should be render show view' do
        get post_path(user_post)

        expect(response).to have_http_status(:success)
        expect(response).to render_template('posts/show')
      end
    end

    it 'Creating a post without user' do
      post posts_path, params: { post: { user_id: user.id, name: 'non auth', body: 'non auth body' } }

      post_test = Post.find_by(name: 'non auth')

      expect(response).to have_http_status(:redirect)
      expect(post_test.blank?).to eq true
    end

    it 'Updating a post without user' do
      expect do
        put post_path(user_post), params: { post: { user_id: user.id, name: 'non auth', body: 'non auth body' } }
      end.not_to(change { user_post.reload.name })

      expect(response).to have_http_status(:redirect)
    end

    it 'Editing a post without user' do
      get edit_post_path(user_post)

      expect(response).to have_http_status(:redirect)
    end

    it 'Destroying a post without user' do
      delete post_path(user_post)

      expect(response).to have_http_status(:redirect)
      expect(user_post.present?).to eq true
    end
  end

  describe 'auth user' do
    let!(:user) { create(:user) }
    let!(:user2) { create(:user, email: 'user2@mail.com') }
    let!(:user_post) { create(:post, user_id: user.id) }
    let!(:user_post2) { create(:post, user_id: user2.id) }

    before :each do
      sign_in user
    end

    it 'should be render index view with auth user' do
      get root_path

      expect(response).to have_http_status(:success)
      expect(response).to render_template('posts/index')
    end

    it 'should be render index with my posts view with auth user' do
      get root_path(my_posts: 'true')

      posts = Post.where(user_id: user.id)
      my_posts = Post.my_posts(user)

      expect(posts).to eq my_posts
      expect(response).to have_http_status(:success)
      expect(response).to render_template('posts/index')
    end

    it 'should be render show view with auth user' do
      get post_path(user_post)

      expect(response).to have_http_status(:success)
      expect(response).to render_template('posts/show')
    end

    it "returns 'The post is not found'" do
      get edit_post_path(id: 696969)

      expect(response).to have_http_status(:redirect)
    end

    it 'Creating a post with auth user' do
      post '/posts', params: { post: { user_id: user.id, name: 'test post', body: 'test body' } }

      expect(Post.last.name).to eq 'test post'
      expect(Post.last.body).to eq 'test body'
    end

    it 'Updating a post with auth user' do
      expect do
        put post_path(user_post), params: { post: { user_id: user.id, name: 'auth', body: 'auth body' } }
      end.to(change { user_post.reload.name })

      expect(response).to have_http_status(:redirect)
      expect(user_post.name).to eq 'auth'
    end

    it 'Editing a post with auth user' do
      get edit_post_path(user_post)

      expect(response).to have_http_status(:success)
      expect(response).to render_template('posts/edit')
    end

    it 'Destroying a post with auth user' do
      expect do
        delete post_path(user_post)
      end.to change(Post, :count).by(-1)

      expect(response).to have_http_status(:redirect)
      expect(Post.exists?(user_post.id)).to be false
    end
  end
end
