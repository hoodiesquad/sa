# frozen_string_literal: true

class CreateActiveAdminComment < ActiveRecord::Migration[7.0]
  def change
    create_table :active_admin_comments do |t|
      t.string :namespace
      t.text :body
      t.references :author, polymorphic: true
      t.references :resource, polymorphic: true
      t.timestamps
    end
  end
end
